d36 = "0123456789abcdefghijklmnopqrstuvwxyz"

def convert_10_to_36(n10):
    r_pos = []
    while True:
        r_pos.append(n10%36)
        n10 = n10//36
        if not n10:break
    return ''.join([d36[i] for i in reversed(r_pos)])

def convert_36_to_10(n36):
    return sum([d36.index(n)*pow(36,power) for power,n in enumerate(reversed(list(n36)))])
